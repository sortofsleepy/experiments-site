import petalFrag from "./shaders/petals/petalFrag";
import petalSim from "./shaders/petals/petalSim";
import petalVert from "./shaders/petals/petalVert";
import createContext from "../libraries/gl/core/gl"
import PetalSystem from "./petalSystem"
export default class PetalContent {

    el:HTMLCanvasElement;
    gl:any;

    // store animation frame id so we can stop it once things are done loading.
    animId:number;

    petalSystem:PetalSystem;

    constructor(el) {

        this.el = el;

        this.gl = createContext(this.el);


        this.petalSystem = new PetalSystem(this.gl,[this.el.width,this.el.height]);
    }

    stop(){
        cancelAnimationFrame(this.animId);
    }

    _setup(){

    }

    start(){this._draw();}

    _draw(){

        let gl = this.gl;
        let animate = () => {
            requestAnimationFrame(animate);
            gl.enableAlphaBlending();
            gl.clearScreen(0.8196078,0.8196078,0.8196078,1.);

            this.petalSystem.draw()

        }

        this.animId = requestAnimationFrame(animate);

    }

}