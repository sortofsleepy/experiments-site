export default `

uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;
uniform float time;
in vec3 position;
in vec4 iPosition;
in float iScale;

out float vAlpha;

uniform bool showProject;

vec3 rotateX(vec3 p, float theta){
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x, p.y * c - p.z * s, p.z * c + p.y * s);
}

vec3 rotateY(vec3 p, float theta) {
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x * c + p.z * s, p.y, p.z * c - p.x * s);
}

vec3 rotateZ(vec3 p, float theta) {
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x * c - p.y * s, p.y * c + p.x * s, p.z);
}
void main(){

    gl_PointSize = 5.0;

    
    float vertexScale = 1.2;


    vec3 pos = position;
    pos.x *= iScale * vertexScale;
    pos.y *= iScale * vertexScale;
    pos.z *= iScale * vertexScale;

    vec3 rotPos = rotateX(pos,time * iScale * 2.0);
    rotPos = rotateY(rotPos,time * iScale * 2.0);
    rotPos = rotateZ(rotPos,time * iScale * 2.0);

    //vec4 pos = vec4(position + iPosition.xyz,1.);
    vec4 finalPos = vec4(rotPos + iPosition.xyz,1.);
    vAlpha = iPosition.a;

    gl_Position = projectionMatrix * modelViewMatrix * finalPos;
    //gl_Position =  vec4(position,1.);

}
`