
export default `
precision highp float;
uniform vec2 resolution;
uniform sampler2D uTex0;
uniform float time;

out vec4 glFragColor;
in float vAlpha;

const vec3 baseColor = vec3(212.0,32.0,133.0);

void main() {
    vec2 uv = gl_FragCoord.xy / resolution;
    vec3 color = normalize(baseColor);

    vec4 dat = texture(uTex0,uv);

    // Time varying pixel color
    vec3 col = 0.5 + 0.5*sin(cos(time) * color);
    col = mix(color,col,0.5);

    dat*=dat;

    glFragColor =  vec4(col,1.);
    glFragColor.a = vAlpha;

}

`