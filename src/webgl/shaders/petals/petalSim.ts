export default `



/**
Transform feedback simulation for moving particle system across the screen
*/

in vec4 position;
in vec4 velocity;
in vec3 origin;

uniform float time;
uniform vec2 mousePos;
out vec4 oPos;
out vec4 oVel;
out vec3 oOrigin;

void main(){
    oPos = position;
    oVel = velocity;
    oOrigin = origin;

    // alpha holds life , when less than 0, reset ps
    if(oPos.a < 0.0){
        oPos.a = 1.0;
        oVel = vec4(0.);
        oPos.xyz = oOrigin;
    }else {

        vec3 mouse = normalize(vec3(mousePos,0.0));
        vec3 curl = curlNoise( oPos.xyz * sin(time));
        oVel.xyz += curl * 0.05;
        oVel *= 0.97;
        oVel.x += 0.01;
        oPos += oVel;
        oPos.a -= 0.01;
    }

}`