import TransformFeedback from "../libraries/gl/framework/TransformFeedback"
import model from "./petalShape"
import createShader from "../libraries/gl//core/shader"
import ObjLoader from "../libraries/gl//loaders/ObjLoader"
import {ShaderFormat} from "../libraries/gl/core/formats"
import Mesh from "../libraries/gl/framework/object"
import {flattenArray, randFloat, randVec4} from "../libraries/gl/math/core"


import petalRenderVert from "./shaders/petals/petalVert"
import petalRenderFrag from "./shaders/petals/petalFrag"
import sim from "./shaders/petals/petalSim"
import noise from "./shaders/noise"
import {PerspectiveCamera} from "../libraries/gl/framework/camera";


export default class ParticleSystem{
    gl:any;
    numParticles:number;
    tf:TransformFeedback;
    renderShader:any;
    petal:Mesh;
    resolution:[number,number];
    camera:PerspectiveCamera;

    showProject:boolean = false;
    mouseOrigin:any;

    constructor(gl,resolution:[number,number],numParticles=350) {

        this.gl = gl;
        this.numParticles = numParticles;

        this.tf = new TransformFeedback(gl,{
            numItems:numParticles
        });

        let simShader = [
            noise,
            sim
        ].join("\n");

        this.resolution = resolution;

        this.camera = new PerspectiveCamera({
            far:10000
        });

        this.camera.setZoom(1030);

        // set transform feedback shader.
        this.tf.setShader(simShader,[
            "oPos",
            "oVel",
            "oOrigin"
        ],["time"]);


        this.renderShader = createShader(gl,new ShaderFormat()
            .vertexSource([
                petalRenderVert
            ].join("\n"))
            .fragmentSource(petalRenderFrag));

        this._loadGeometry()

        window.addResizeCallback(()=>{
            this.resize(this.gl.canvas.width,this.gl.canvas.height);
        })

    }

    toggleProject(){
        this.showProject = !this.showProject;
    }

    resize(w,h){
        this.resolution = [w,h];
        this.camera.resize(w / h);
    }

    draw(mousePos=[0,0]){
        let perf = performance.now() * 0.05;
        let camera = this.camera;
        // TODO enable alpha blending

        // update transform feedback
        this.tf.update();
        this.tf.shader.float("time",performance.now() * 0.0005);



        this.petal.draw(camera);
        this.petal.shader.float("time",perf);


        this.petal.uniform("projectionMatrix",camera.projectionMatrix)
            .uniform("modelViewMatrix",camera.viewMatrix)
            .uniform("resolution",this.resolution)
            .uniform("showProject",this.showProject)


        this.petal.shader.int("uTex0",0);
        this.gl.unbindTextures();
    }

    drawTest(){
        let perf = performance.now() * 0.05;
        let camera = this.camera;
        // TODO enable alpha blending

        // update transform feedback
        this.tf.update();
        this.tf.shader.float("time",performance.now() * 0.0005);



        this.petal.draw(camera);
        this.petal.shader.float("time",perf);


        this.petal.uniform("projectionMatrix",camera.projectionMatrix)
            .uniform("modelViewMatrix",camera.viewMatrix)
            .uniform("resolution",this.resolution)


        this.petal.shader.int("uTex0",0);
        this.gl.unbindTextures();
    }

    _build(){

        let positions = [];
        let velocity = [];
        let originData = [];

        for(let i = 0; i < this.numParticles; ++i){

            // setup particle position
            let position = [-20,0,100,randFloat()]

            // setup particle velocity
            let vel = [randFloat(),0,0,0]

            positions.push(...position);
            velocity.push(...vel);
            originData.push(position[0],position[1],position[2]);
        }

        // add attributes to keep track of.
        // note - position w will hold particle life.
        this.tf.addAttribute("position",positions,{
            size:4
        });
        this.tf.addAttribute("velocity",velocity,{
            size:4
        });

        this.tf.addAttribute("origin",originData);

    }

    /**
     * Load petal geometry
     * @param assets
     * @private
     */
    _loadGeometry(){


        this._build();
        let petalGeo = ObjLoader.parse(model);
        // flatten arrays
        let positions = flattenArray(petalGeo.positions);
        let uvs = flattenArray(petalGeo.coords,2);

        // setup random scale
        let scale = [];
        for(let i = 0; i < this.numParticles;++i){
            scale.push(Math.random() + 1.0);
        }


        this.petal = new Mesh(this.renderShader)
        this.petal.addAttribute("position",positions);
        this.petal.addInstancedAttributeBuffer("iPosition",this.tf.getAttributeData("position"),{
            size:4,
            numItems:100
        });
        this.petal.addInstancedAttribute("iScale",scale,{
            size:1
        });


        this.petal.numItems = this.numParticles;

    }
}