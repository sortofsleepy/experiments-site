/**
 * Ensures a value lies in between a min and a max
 * @param value
 * @param min
 * @param max
 * @returns {*}
 */
export function clamp(value:number,min:number,max:number){
    return min < max
        ? (value < min ? min : value > max ? max : value)
        : (value < max ? max : value > min ? min : value)
}
