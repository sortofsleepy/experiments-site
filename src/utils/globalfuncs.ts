/**
 * Enables ability to consolidate something's resize listener within just one event listener.
 */
export function enableGlobalResize(){
    window.resizeCallbacks = [];
    window.addResizeCallback = (cb) => {
        window.resizeCallbacks.push(cb);
    }

    window.addEventListener("resize",(e) => {
        window.resizeCallbacks.forEach(cb => {
            cb(e)
        })
    })
}



export function enableGlobalAnimationListener(){
    window.animationCallbacks = [];

    window.startAnimationFrame = ()=> {
        let animate = ()=>{
            requestAnimationFrame(animate);

            window.animationCallbacks.forEach(cb=>{
                cb()
            })
        }
        window.animationId = requestAnimationFrame(animate);
    }

    window.addAnimationCallback = (cb)=> {
        window.animationCallbacks.push(cb);
    }

    window.stopAnimationFrame = ()=> {
        cancelAnimationFrame(window.animationId);
    }
}