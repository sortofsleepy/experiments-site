/**
 * A barebones image resizer.
 */
export default class ImageResizer {

    el:HTMLCanvasElement;
    ctx:CanvasRenderingContext2D;
    dimensions:any;
    crop:any;

    toDataURL:boolean = false;

    // images converted to blobs are stored here.
    imageCache:Array<any> = [];
    constructor({
        shouldReturnDataURL = false,
        targetDimensions = {
            width:640,
            height:480,
            cropWidth:0,
            cropHeight:0
        }
                }={}) {

        this.el = document.createElement("canvas");
        this.el.width = targetDimensions.width;
        this.el.height = targetDimensions.height;
        this.ctx = this.el.getContext("2d");

        this.dimensions = [targetDimensions.width,targetDimensions.height];
        this.crop = [targetDimensions.cropWidth,targetDimensions.cropHeight];

        this.toDataURL = shouldReturnDataURL;
    }

    /**
     * Resizes a series of images
     * Returns either an array of data urls or Promises.
     * @param imgs {Array} an array of image elements.
     */
    resizeImages(imgs:Array<HTMLImageElement>){
        return imgs.map(img => {
            return this.resizeImage(img);
        })
    };

    /**
     * Resizes an image to fit the target dimensions.
     * Returns resized image either as a data url or a Promise depending on whether or not
     * toDataURL is true/false.
     * Based on
     * https://stackoverflow.com/questions/31778559/how-do-i-proportionally-resize-a-video-in-canvas
     * and the original teachable machine code.
     *
     * @param img {HTMLImageElement} an Image element
     */
    resizeImage(img:HTMLImageElement){

        let ctx = this.ctx;

        ctx.clearRect(0,0,this.dimensions.width,this.dimensions.height);

        const cropWidth = this.crop[0] !== 0 ? this.crop[0] : 640
        const cropHeight = this.crop[1] !== 0 ? this.crop[1] : 480
        const cropTop = 0

        const t = img.width;
        const e = img.height;

        const n = t / this.dimensions[0]
        const r = 40
        const cropLeft = (this.dimensions[0] - cropWidth) / 2

        /////////////////////
        const vidX = t - cropLeft * n - cropWidth * n
        const vidY = cropTop * n
        const vidWidth = cropWidth * n
        const vidHeight = cropHeight * n

        const hRatio = (this.dimensions[0] / vidWidth) * vidHeight

        this.ctx.drawImage(img,0,0,this.dimensions[0],hRatio);

        if(!this.toDataURL){
            return new Promise((res,rej)=>{
                this.el.toBlob((blob)=>{
                    let url = URL.createObjectURL(blob);
                    this.imageCache.push(url);
                    res(url);
                })
            })
        }else{
            return this.el.toDataURL();
        }

    }

}