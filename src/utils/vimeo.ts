/**
 * Builds vimeo embed code.
 * @param video_id {string} id of video to embed.
 */
export default function(video_id:string){
    return `
    <div id="VIMEO_EMBED" style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/${video_id}?h=a907b69632&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title=""></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
    `
}