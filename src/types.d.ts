interface Window {
    addAnimationCallback:any;
    startAnimationFrame:any;
    stopAnimationFrame:any;
    animationId:any;
    animationCallbacks:Array<any>;

    // for resizing
    addResizeCallback:any;
    resizeCallbacks:Array<any>;
}

interface WebGL2RenderingContext {
    clearScreen:any;
    enableAlphaBlending:any;
}