import "./css/main.scss"
import App from "./components/app.svelte"
import {enableGlobalAnimationListener,enableGlobalResize} from "./utils/globalfuncs";


// init animation frame helper.
enableGlobalAnimationListener();
window.startAnimationFrame();

// enable global resize listeners
enableGlobalResize();

// init app
let app = new App({
    target:document.body
})

export default app;

if(import.meta.hot){
    import.meta.hot.accept();
    import.meta.hot.dispose(() => {
        app.$destroy();
    })
}