import mat4 from '../math/mat4'
import vec3 from '../math/vec3'
import {Camera} from "../interfaces";
import {toRadians} from "../math/core";

/**
 * The basic components of a camera
 */
export class CameraBase implements Camera{
    projectionMatrix:Array<any> = mat4.create();
    viewMatrix:Array<any> = mat4.create();
    near:number;
    far:number;
    fov:number;
    aspect:number;
    eye:Array<any> = vec3.create();
    target:Array<any> = vec3.create();
    center:Array<any> = vec3.create();
    position:Array<any> = vec3.create();
    up:Array<any> = vec3.create();
    zoom:number;

    frustumTop:any;
    frustumBottom:any;
    frustumRight:any;
    frustumLeft:any;
    constructor() {

        this.up[1] = 1;
        mat4.identity(this.viewMatrix);
        mat4.identity(this.projectionMatrix);
        this.setZoom(1);
    }

    translate(position:[number,number,number]){
        this.eye[0] = position[0];
        this.eye[1] = position[1];

        // so we can maintain z position, z position will stay the same if position parameter z position is 0
        this.eye[2] = position[2] !== 0 ? position[2] : this.eye[2];
        mat4.identity(this.viewMatrix);
        mat4.lookAt(this.viewMatrix,this.eye,this.center,this.up)
    }

    cameraLookAt(){
        mat4.lookAt(this.viewMatrix,this.eye,this.center,this.up);
    }

    calcFrustum(){
        this.frustumTop		=  this.near * Math.tan( 3.14149/ 180.0 * this.fov * 0.5 );
        this.frustumBottom	= -this.frustumTop;
        this.frustumRight	=  this.frustumTop * this.aspect
        this.frustumLeft	= -this.frustumRight;
    }

    /**
     * Creates a projection matrix for the purposes of picking objects in an
     * FBO
     * https://webgl2fundamentals.org/webgl/lessons/webgl-picking.html
     * @param pixelX
     * @param pixelY
     * @param gl
     */
    createPickingProjection(pixelX:number,pixelY:number,gl){
        this.calcFrustum();
        const aspect = this.aspect;
        const top = Math.tan(this.fov * 0.5) * this.near;
        const bottom = -top;
        const left = aspect * bottom;
        const right = aspect * top;
        const width = Math.abs(right - left);
        const height = Math.abs(top - bottom);

        const subLeft = left + pixelX * width / gl.canvas.width;
        const subBottom = bottom + pixelY * height / gl.canvas.height;
        const subWidth = 1 / gl.canvas.width;
        const subHeight = 1 / gl.canvas.height;

        return mat4.frustum(mat4.create(),subLeft,subLeft + subWidth,subBottom, subBottom + subHeight, this.near, this.far)
    }

    calcRay(uPos,vPos,imagePlaneAspectRatio){
        let s = ( uPos - 0.5 + 0.5 ) * imagePlaneAspectRatio;
        let t = ( vPos - 0.5 + 0.5 );
        let viewDistance = imagePlaneAspectRatio / Math.abs( this.frustumRight - this.frustumLeft ) * this.near;
    }

    /**
     * Returns inverted matrix of projectionMatrix * view matrix
     * Useful for Raycasting
     */
    getInverseProjectionView(){
        let inverse = mat4.create();
        mat4.multiply(inverse,this.projectionMatrix,this.viewMatrix);
        mat4.invert(inverse,inverse);
        return inverse;
    }

    getInverseProjection(){
        let inverse = mat4.create();
        mat4.invert(inverse,this.projectionMatrix);
        return inverse;
    }

    getInverseView(){
        let inverse = mat4.create();
        mat4.invert(inverse,this.viewMatrix);
        return inverse;
    }

    getX(){ return this.eye[0]; }
    getY(){ return this.eye[1]; }
    getZ(){ return this.eye[2]; }

    setZoom(val:number){
        this.eye[2] = val;

        mat4.lookAt(this.viewMatrix,this.eye,this.center,this.up)
    }

}

/*
 setPosition(x,y){
        this.eye[0] = x;
        this.eye[1] = y;

        this.viewMatrix = mat4.lookAt(this.viewMatrix,this.eye,this.center,this.up);
    }

    translate(vector:Array<any>,target:[number,number,number] = [0,0,0]){

        this.position[0] = vector[0];
        this.position[1] = vector[1];
        this.position[2] = vector[2];

        this.cameraLookAt(this.eye,target);
        return this;
    }


    cameraLookAt(eye:Array<number>,aCenter?:[number,number,number]){
        this.eye = vec3.clone(eye);
        this.center = aCenter !== undefined ? vec3.clone(aCenter) : this.center;

        vec3.copy(this.position,eye);
        mat4.identity(this.viewMatrix);
        mat4.lookAt(this.viewMatrix,eye,this.center,this.up)
        return this;
    }

    setZoom(zoom:number){
        this.zoom = zoom;
        this.position = [0,0,zoom];

        this.cameraLookAt([0,0,0])
        mat4.translate(this.viewMatrix,this.viewMatrix,[0,0,zoom]);

        return this;
    }
 */

/**
 * The basic definition of a PerspectiveCamera
 */
export class PerspectiveCamera extends CameraBase{

    constructor({
                    fov = Math.PI / 4,
                    aspect = window.innerWidth / window.innerHeight,
                    near = 0.1,
                    far = 1000.0
                }={}) {
        super();

        this.projectionMatrix = mat4.create();
        this.viewMatrix = mat4.create();
        this.fov = toRadians(fov);
        this.aspect = aspect;
        this.near = near;
        this.far = far;


        mat4.perspective(this.projectionMatrix,this.fov,aspect,near,far);

        return this;
    }

    /**
     * Same as resize function
     * @param aspectRatio {number} the new aspect ratio
     */
    updateProjectionMatrix(aspectRatio:number){
        this.resize(aspectRatio);
        return this;
    }

    /**
     * Resizes the projection matrix based on a new aspect ratio.
     * @param aspectRatio {number} the new aspect ratio
     */
    resize(aspectRatio:number){
        this.aspect = aspectRatio;
        mat4.perspective(this.projectionMatrix,this.fov,this.aspect,this.near,this.far);

        return this;
    }


}

/**
 * Basic components of an orthographic camera
 */
//https://webglfundamentals.org/webgl/lessons/webgl-3d-orthographic.html
//http://learnwebgl.brown37.net/08_projections/projections_ortho.html
/*
useable projection for 2D elements.
camera.projectionMatrix = [
    2 / width, 0, 0, 0,
    0, -2 / height, 0, 0,
    0, 0, 2 / depth, 0,
    -1, 1, 0, 1,
];

 */
export class OrthoCamera extends CameraBase{

    left:number;
    right:number;
    bottom:number;
    top:number;
    near:number;
    far:number;

    constructor({
                    left=0,
                    right=window.innerWidth,
                    top=0,
                    bottom=window.innerHeight,
                    near=0,
                    far=100000

    }={}) {
        super();
        this.projectionMatrix = mat4.create();
        this.viewMatrix = mat4.create();

        this.left = left;
        this.right = right;
        this.bottom = bottom;
        this.top = top;
        this.near = near;
        this.far = far;

        this.center = [0,0,-1];

        this._update();


      //  mat4.translate(this.viewMatrix,this.viewMatrix,[0,0,-20000])
        //mat4.lookAt(this.viewMatrix,[0.0,0,0],[0,0,0],[0,0,0])

        return this;
    }

    scaleViewVolume(){
        mat4.mul
    }

    setZoom(val: number) {
        super.setZoom(val);
        this._update();
    }

    setLeft(num:number){
        this.left = num;
        this._update();
        return this;
    }

    setRight(num:number){
        this.right = num;
        this._update();
        return this;
    }
    setBottom(num:number){
        this.bottom = num;
        this._update();
        return this;
    }
    setTop(num:number){
        this.top = num;
        this._update();
        return this;
    }

    setNear(num:number){
        this.near = num;
        this._update();
        return this;
    }

    setFar(num:number){
        this.far = num;
        this._update();
        return this;
    }

    _update(){


        mat4.ortho(this.projectionMatrix,this.left,this.right,this.bottom,this.top,this.near,this.far);

    }

}