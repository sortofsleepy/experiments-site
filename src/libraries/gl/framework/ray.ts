import vec3 from "../math/vec3";
import vec4 from "../math/vec4";

/**
 * A port of the Cinder Ray class.
 */
export default class Ray {

    origin:Array<any> = vec4.create();
    direction:Array<any> = vec4.create();
    inverseDirection:Array<any> = vec4.create();

    signX:number = 0;
    signY:number = 0;
    signZ:number = 0;

    constructor(origin=vec4.create(),direction=vec4.create()) {
        this.origin = origin;
        this.direction = direction;
    }

    setDirection(direction){
        this.inverseDirection = vec4.fromValues(1.0 / this.direction[0],1.0 / this.direction[1], 1.0 / this.direction[2]);
        this.signX = ( this.direction[0] < 0.0 ) ? 1 : 0;
        this.signY = ( this.direction[1] < 0.0 ) ? 1 : 0;
        this.signZ = ( this.direction[2] < 0.0 ) ? 1 : 0;
    }

    getDirection() {return this.direction;}
    getInverseDirection() { return this.inverseDirection;}

    getSignX(){return this.signX;}
    getSignY(){return this.signY;}
    getSignZ(){return this.signZ;}

    transform(matrix:Array<number>){
        // TODO
    }

    setFromOriginMatrixPosition(mat:Array<number>){

        this.origin[0] = mat[12];
        this.origin[1] = mat[13];
        this.origin[2] = mat[14];

    }

    applyMat4(m){

    }

    calcPosition(t){
        let origAndDir = vec4.create();
        vec3.add(origAndDir,this.origin,this.direction);
        return vec3.multiplyScalar(origAndDir,origAndDir,t);
    }

    /**
     *
     * @param origin {Vec3} plane origin
     * @param normal {Vec3} plane normal
     */
    calcPlaneIntersection( origin, normal){

        let denom = vec3.dot(normal,this.getDirection());
        if(denom != 0.0){
            return true
            // return vec3.dot(normal,vec3.subtract(origin,this.origin)) / denom;
        }else{
            return false;
        }
    }
}

///// possibly useful code later ////

/*

   let toWorld = this.camera.getInverseProjectionView();

   let from = vec4.fromValues(mouse.x,mouse.y,-1.0,1.0);
   vec4.transformMat4(from, from,toWorld);

   let to = vec4.fromValues(mouse.x,mouse.y,1.0,1.0);
   vec4.transformMat4(to, to,toWorld);
   const startSign = this.distanceToPoint( from );
   const endSign = this.distanceToPoint(to );

   console.log(startSign,endSign)
   if (( startSign < 0 && endSign > 0 ) || ( endSign < 0 && startSign > 0 )){
       console.log("found")
   }
    */
/*
  let invProjMat = this.camera.getInverseProjectionView();
this.ray.setFromOriginMatrixPosition(this.camera.viewMatrix)

this.ray.direction[0] = mouse.x;
this.ray.direction[1] = mouse.y;
this.ray.direction[2] = 0.5;


this.ray.direction = this._applyMat4(this.ray.direction,this.camera.getInverseProjection());
this.ray.direction = this._applyMat4(this.ray.direction,this.camera.getInverseView());

this.ray.direction = vec4.subtract(this.ray.direction,this.ray.direction,this.ray.origin);
this.ray.direction = vec4.normalize(this.ray.direction,this.ray.direction);

console.log(this.ray.direction)

// vec4.transformMat4(this.ray.direction,this.ray.direction,invProjMat);

console.log(this.ray.direction)
 */


/*
 let p = vec3.create();
vec3.subtract(p,part.position,from);


 let intersectTest = this._intersectSphere(p,direction,0.1);

if(intersectTest){
    console.log("found")
}

  //https://stackoverflow.com/questions/20140711/picking-in-3d-with-ray-tracing-using-ninevehgl-or-opengl-i-phone/20143963#20143963
    _raycast(){
        let mouse = this.mousePosition;

        let toWorld = this.camera.getInverseProjectionView();

        let from = vec4.fromValues(mouse.x,mouse.y,-1.0,1.0);
        vec4.transformMat4(from, from,toWorld);

        let to = vec4.fromValues(mouse.x,mouse.y,1.0,1.0);
        vec4.transformMat4(to, to,toWorld);

        from[0] /= from[3];
        from[1] /= from[3];
        from[2] /= from[3];
        from[3] /= from[3];

        to[0] /= to[3];
        to[1] /= to[3];
        to[2] /= to[3];
        to[3] /= to[3];

        let minDist = 99999.0;
        from = vec3.fromValues(from[0],from[1],from[2]);
        to = vec3.fromValues(to[0],to[1],to[2]);

        this.particles.forEach((part,i) => {
            let direction = vec3.create();
            vec3.subtract(direction,to,from);


            let position = part.position;
            let normal = [0,1,0];

            let s = this._intersectTest(position,normal,to);

        })
    }

    _intersectTest(origin,normal,rayDirection){

        let denom = vec3.dot(normal,rayDirection);
        console.log(denom)
        if(denom != 0.0){
            return true
            // return vec3.dot(normal,vec3.subtract(origin,this.origin)) / denom;
        }else{
            return false;
        }
    }

    _intersectSphere(p:[number,number,number],d:[number,number,number],r:number){
        let A = vec3.dot(d,d);
        let B = 2.0 * vec3.dot(d,p);
        let C = vec3.dot(p,p) - r * r;

        let dis = B * B - 4.0 * A - C;
        if(dis < 0.0){
            return false;
        }

        let S = Math.sqrt(dis);
        let t1 = (-B - S) / (2.0 * A);
        let t2 = (-B + S) / (2.0 * A);

        return true;
    }

 */
