import {Geometry} from "../interfaces";
export default class Quad implements Geometry {
    vertices:Array<any> = [-1, -1, -1, 4, 4, -1];
    normals:Array<any> = [];
    indices:Array<any> = [];
    uvs:Array<any> = [];
    customSize:number = 2;

    constructor() {}

}

