precision highp float;
uniform sampler2D inputTexture;
in  vec2 vUv;
out vec4 glFragColor;

void main(){

    vec4 data = texture(inputTexture,vUv);
    glFragColor = data;
}