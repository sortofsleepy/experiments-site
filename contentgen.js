const fs = require("fs");

/**
 * Purpose of this file is to build a simple manifest of how to treat content and how to link out to other areas if necessary.
 * @type {string}
 */

const BASE_PATH = "./public/content"
const PUBLIC_PATH = "./content"

const dirs = fs.readdirSync(BASE_PATH)

let datafile = dirs.map(dir => {
    const path = `${BASE_PATH}/${dir}`
    const metadata_raw = fs.readFileSync(`${path}/metadata.json`,"utf8")
    let metadata = JSON.parse(metadata_raw);

    /**
     * General data structure
     *
     *  All items should have
     *  - id
     *  - title
     *  - url
     *
     *  - if url leads to an external site, it should be marked as external.
     *  - url will be constructed here for non-external sites, otherwise it should be provided.
     *  - if item is webgpu, should be marked as webgpu so we can hide on non webgpu capable browsers.
     *  - if item is Vimeo, url should contain id.
     */

    /// first check to ensure all property fields exist.
    let keys = ["id","title"];
    keys.forEach(key => {
        if(!metadata.hasOwnProperty(key)){
            throw new Error(`metadata for ${path} does not contain the property ${key}`);
        }
    })


    // construct url
    if(metadata.hasOwnProperty("external")){
        if(!metadata.hasOwnProperty("url")){
            throw new Error(`Item ${metadata.id} is marked as external but provides no url.`)
        }else{

            if(metadata.url.search("http") === -1 &&
                !metadata.hasOwnProperty("vimeo")){
                throw new Error(`Item ${metadata.id} is marked as external but the url appears to lead to a local path`)
            }

        }
    }else{
        metadata.url = `${PUBLIC_PATH}/${dir}/index.html`
    }



    return metadata;
})

// write data to ts file as a module we can reference.
fs.writeFileSync("./src/data.ts",`export default ${JSON.stringify(datafile)}`)